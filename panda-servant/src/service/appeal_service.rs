use super::super::ResponseEntity;
use panda_storage::model::appeal::Appeal;
///
pub fn select_by_id<'a>(id: u16) -> ResponseEntity<'a, Appeal> {
    let option = Appeal::select_by_id(id);
    ResponseEntity::success().data(option)
}
///
pub fn insert<'a>(s: String) -> ResponseEntity<'a, u16> {
    let appeal = Appeal {
        id: 0,
        appeal_name: Some(s),
    };
    let i = Appeal::insert(appeal);
    ResponseEntity::success().data(Some(i))
}
