use std::fs;
use std::env;
use std::path::Path;

fn main() {
    println!("build.rs run");
    let mut out_dir = env::var("OUT_DIR").unwrap();
    let path = Path::new(&out_dir).parent().unwrap().parent().unwrap().parent().unwrap();
    let target = path.join("config.toml");
    println!("target = {}",target.to_str().unwrap());
    let cargo_manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let source = Path::new(&cargo_manifest_dir).parent().unwrap().join("config.toml");
    println!("source = {}",source.to_str().unwrap());

    let buf = path.join("config.toml");
    fs::copy(source.as_path(), target.as_path()).unwrap();
}
