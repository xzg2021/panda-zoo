use actix_web::{error, web, App, FromRequest, HttpResponse, HttpServer};
use panda_storage::db_source;
mod router_service;
use actix_rt;
use panda_servant::ResponseEntity;
use serde::{Deserialize, Serialize};
use serde_json;
use std::io;
fn main() -> io::Result<()> {
    let sys = actix_rt::System::new("example");
    db_source::init_mysql_ds();

    println!("hahaha");
    HttpServer::new(
        || App::new().configure(router_service::appeal_router::config_service), //        .service(router_service::appeal_router::select_by_id)
                                                                                //        .data(router_service::appeal_router::select_by_id_error_response())
                                                                                //
                                                                                //        .service(router_service::appeal_router::insert)
                                                                                //        .data(router_service::appeal_router::insert_error_response())
    )
    .bind("127.0.0.1:8088")?
    .start();
    //    actix_rt::System::current().stop();
    sys.run()
}
