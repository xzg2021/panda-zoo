use actix_web::{error, get, http, post, web, FromRequest, HttpRequest, HttpResponse, Responder};
use panda_servant::{self, service::appeal_service};
use serde::{Deserialize, Serialize};
///module route
pub fn config_service(cfg: &mut web::ServiceConfig) {
    cfg.service(insert)
        .data(insert_error_response())
        .service(select_by_id)
        .data(select_by_id_error_response());
}
///添加
#[post("appeal/insert")]
fn insert(json: web::Json<AddAppealReq>) -> impl Responder {
    println!("{}", json.appeal_name);
    appeal_service::insert(json.appeal_name.to_owned())
}
#[derive(Deserialize)]
struct AddAppealReq {
    appeal_name: String,
}
fn insert_error_response() -> impl Default + 'static {
    web::Json::<AddAppealReq>::configure(|cfg| {
        cfg.error_handler(|err, _req| {
            error::InternalError::from_response(
                err,
                panda_servant::ResponseEntity::error_response(),
            )
            .into()
        })
    })
}

///查询
#[get("appeal/select_by_id")]
fn select_by_id(info: web::Query<Info>) -> impl Responder {
    println!("req id={}", info.id);
    appeal_service::select_by_id(info.id)
}
#[derive(Deserialize)]
struct Info {
    id: u16,
}
fn select_by_id_error_response() -> impl Default + 'static {
    web::Query::<Info>::configure(|cfg| {
        cfg.error_handler(|err, _req| {
            error::InternalError::from_response(
                err,
                panda_servant::ResponseEntity::error_response(),
            )
            .into()
        })
    })
}
