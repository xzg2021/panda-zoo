# panda-zoo

#### 介绍
Rust实现使用actix-web库实现的一个带操作mysql的简单web demo

#### 软件架构
1. panda-app:服务启动功能,路由服务注册功能
2. panda-servant:service业务处理模块
3. panda-storage:数据落地持久模块

#### 安装教程

1. 克隆项目到本地
2. 阅读文件目录中gd_bill_appeal.sql步骤并执行
3. 配置config.toml
4. cargo build项目
5. 在target目录找到执行文件并执行
6. 使用postman发起添加数据测试请求:
   1. method: post
   2. url: http://127.0.0.1:8088/appeal/insert,
   3. 请求体参数: {"appeal_name":"退款"}
   4. 一切正常的话可以看到数据已经添加
7. 使用postman对刚才添加的数据根据主键进行查询:
   1. method: get
   2. url: http://127.0.0.1:8088/appeal/select_by_id?id=1
   3. 一切正常的话可以看到查询到的数据包装成json返回

#### Rust web改造传统 java web优缺点
#####优点:

1. 执行文件体积小,占用资源少,是成几何倍的少,性能快,飞一般的感觉
2. 项目管理是工业级的,语言天生自带的特性注释即文档,项目管理,代码质量管理
   等,这是Rust语言作为新世纪工业级语言优势,项目管理不会失控,还有很多特性可
   以自行了解
3. 我对我的代码自信了,编译通过运行了就几乎不存在空指针,内存泄漏,内存溢出
   等问题,程序跑得非常稳定,直到你服务器关机
4. 还有很多。。。。

######缺点:

1. 学习难度陡峭
2. 还是学习难度陡峭
3. 但是随着编译器越来越智能和你掌握Rust越来越深,效率会越来越快,或者换句
   话说作为项目的负责人或者开发人员,只要编译过了,你的代码就是85分了,总
   比天天代码review还提心吊胆的好
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)