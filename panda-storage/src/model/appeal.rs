use crate::db_source::get_mysql_conn_pool;
use mysql::{Opts, OptsBuilder,Params, Pool};
use mysql_common::params;
use serde::Serialize;
use std::collections::HashMap;
// use

#[derive(Serialize)]
pub struct Appeal {
    pub id: u16,
    pub appeal_name: Option<String>,
}

impl Appeal {
    ///new
    pub fn new(id: u16, appeal_name: Option<String>) -> Appeal {
        Appeal { id, appeal_name }
    }
    ///insert

    pub fn insert(appeal: Appeal) -> u16 {
        let mysql_conn_pool = get_mysql_conn_pool().unwrap();
        let sql = r"INSERT INTO dev_order.gd_bill_appeal
        (appeal_name)
        VALUES
        (:appeal_name)";
        let result = mysql_conn_pool
            .prep_exec(sql, params! {"appeal_name" => appeal.appeal_name})
            .expect("");
        1
    }
    ///select_by_id
    pub fn select_by_id(id: u16) -> Option<Appeal> {
        let mysql_conn_pool = get_mysql_conn_pool().unwrap();
        let sql = "SELECT id, appeal_name from dev_order.gd_bill_appeal where id = :id";
        let p = params!("id" => id);
        let mut selected: Vec<Appeal> = mysql_conn_pool
            .prep_exec(sql, p)
            .map(|result| {
                result
                    .map(|x| x.unwrap())
                    .map(|row| {
                        let (id, appeal_name) = mysql::from_row(row);
                        Appeal { id, appeal_name }
                    })
                    .collect()
            })
            .unwrap();
        let option = selected.pop();
        option
    }
}
